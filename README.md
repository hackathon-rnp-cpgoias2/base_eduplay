# Base do Eduplay

> Olá, pequeno Padawan!

Este projeto consiste num conjunto de facilitadores e aceleradores para o desenvolvimento do desafio proposto na [Hackathon da RNP (projeto Eduplay) na Campus Party Goiás 2](https://cpgoias2.vaitercampus.org/#/paginas/hackathon-rnp).

Para mais recursos, dirija-se ao [repositório da aplicação de exemplo](https://gitlab.com/hackathon-rnp-cpgoias2/app_exemplo).
