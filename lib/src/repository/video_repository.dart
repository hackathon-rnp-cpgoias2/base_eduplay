import 'dart:developer';

import 'package:dio/dio.dart';
import '../model/video.dart';
import '../model/enums.dart';

class VideoRepository {

  final Dio _dio;
  VideoRepository(this._dio);

  Future<List<Video>> fetchVideos({SortType sort = SortType.RELEVANT}) async {
    try {
      final response = await _dio.get('/video', queryParameters: {'order': sort.index, 'offet': 0, 'limit': 100});
      if (response.statusCode != null && response.statusCode! >= 200 && response.statusCode! < 300) {
        final page = VideoResult.fromJson(response.data);
        return page.items ?? [];
      }
    }catch (e) {
      log('Erro ao obter vídeos', error: e);
    }
    return [];
  }
  
  Future<VideoVersion?> versionFor({required int videoId}) async {
    final response = await _dio.get('/video/versions/$videoId');
    if (response.statusCode != null && response.statusCode! >= 200 && response.statusCode! < 300) {
      final map = response.data as Map<String, dynamic>;
      final versions = (map["videoVersionList"] as List<dynamic>).map((e) => VideoVersion.fromJson(e as Map<String, dynamic>)).toList(growable: false);
      if (versions.length > 1) {
        versions.sort((a, b) => (a.bitRate ?? 0).compareTo(b.bitRate ?? 0));
        return versions.last;
      }
      return versions.first;
    }
    return null;
  }

  Future<void> registerPlayback(int videoId) async {
    await _dio.post('/video/$videoId/view');
  }

}
