import 'dart:convert';

class Institution {
  Institution({
    this.id,
    this.name,
    this.code,
  });

  int? id;
  String? name;
  String? code;

  factory Institution.fromRawJson(String str) => Institution.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Institution.fromJson(Map<String, dynamic> json) => Institution(
    id: json["id"],
    name: json["name"],
    code: json["code"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "code": code,
  };
}