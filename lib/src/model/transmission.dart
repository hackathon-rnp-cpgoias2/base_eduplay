import 'dart:convert';

import 'channel.dart';
import 'enums.dart';
import 'institution.dart';
import 'schedule.dart';


class TransmissionResult {
  TransmissionResult({
    this.qtTotal,
    this.items,
  });

  int? qtTotal;
  List<Transmission>? items;

  factory TransmissionResult.fromRawJson(String str) => TransmissionResult.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory TransmissionResult.fromJson(Map<String, dynamic> json) => TransmissionResult(
    qtTotal: json["qtTotal"],
    items: json["transmissionList"] == null ? null : List<Transmission>.from(json["transmissionList"].map((x) => Transmission.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "qtTotal": qtTotal,
    "transmissionList": items?.map((e) => e.toJson()),
  };
}

class Transmission {
  Transmission({
    this.id,
    this.title,
    this.description,
    this.keywords,
    this.author,
    this.link,
    this.contact,
    this.eventName,
    this.eventLocation,
    this.url,
    this.passwordUrlTransmission,
    this.audienceExpected,
    this.transmissionQuality,
    this.visibility,
    this.passwordUrlAccess,
    this.friendlyUrl,
    this.codeWidgetTwitter,
    this.chatType,
    this.chatModeratorEmail,
    this.viewPageUrl,
    this.domainsEmbed,
    this.schedules,
    this.streamType,
    this.institution,
    this.apiHost,
    this.sharedSecret,
    this.meetingId,
    this.record = false,
    this.geolocationControl,
    this.channels,
  });

  int? id;
  String? title;
  String? description;
  String? keywords;
  String? author;
  String? link;
  String? contact;
  String? eventName;
  String? eventLocation;
  String? url;
  String? passwordUrlTransmission;
  AudienceExpected? audienceExpected;
  String? transmissionQuality;
  Visibility? visibility;
  String? passwordUrlAccess;
  String? friendlyUrl;
  String? codeWidgetTwitter;
  ChatType? chatType;
  String? chatModeratorEmail;
  String? viewPageUrl;
  List<dynamic>? domainsEmbed;
  List<Schedule>? schedules;
  StreamType? streamType;
  Institution? institution;
  String? apiHost;
  String? sharedSecret;
  String? meetingId;
  bool record;
  GeolocationControl? geolocationControl;
  List<Channel>? channels;

  factory Transmission.fromRawJson(String str) => Transmission.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Transmission.fromJson(Map<String, dynamic> json) => Transmission(
    id: json["id"],
    title: json["title"],
    description: json["description"],
    keywords: json["keywords"],
    author: json["author"],
    link: json["link"],
    contact: json["contact"],
    eventName: json["eventName"],
    eventLocation: json["eventLocation"],
    url: json["url"],
    passwordUrlTransmission: json["passwordURLTransmission"],
    audienceExpected: json["audienceExpected"] != null ? AudienceExpected.values.byName(json["audienceExpected"]) : null,
    transmissionQuality: json["transmissionQuality"],
    visibility: json["visibility"] != null ? Visibility.values.byName(json["visibility"]) : null,
    passwordUrlAccess: json["passwordURLAccess"],
    friendlyUrl: json["friendlyUrl"],
    codeWidgetTwitter: json["codeWidgetTwitter"],
    chatType: json["chatType"] != null ? ChatType.values.byName(json["chatType"]) : null,
    chatModeratorEmail: json["chatModeratorEmail"],
    viewPageUrl: json["viewPageUrl"],
    domainsEmbed: json["domainsEmbed"],
    schedules: json["schedules"] == null ? null : List<Schedule>.from(json["schedules"].map((x) => Schedule.fromJson(x))),
    streamType: json["streamType"] != null ? StreamType.values.byName(json["streamType"]) : null,
    institution: json["institution"] != null ? Institution.fromJson(json["institution"]) : null,
    apiHost: json["apiHost"],
    sharedSecret: json["sharedSecret"],
    meetingId: json["meetingID"],
    record: json["record"],
    geolocationControl: json["geolocationControl"] != null ? GeolocationControl.values.byName(json["geolocationControl"]) : null,
    channels: (json["channels"] != null && json["channels"] is List<Map<String, dynamic>>) ? (json["channels"] as List<Map<String, dynamic>>).map(Channel.fromJson).toList() : null,
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "description": description,
    "keywords": keywords,
    "author": author,
    "link": link,
    "contact": contact,
    "eventName": eventName,
    "eventLocation": eventLocation,
    "url": url,
    "passwordURLTransmission": passwordUrlTransmission,
    "audienceExpected": audienceExpected?.toString(),
    "transmissionQuality": transmissionQuality,
    "visibility": visibility?.toString(),
    "passwordURLAccess": passwordUrlAccess,
    "friendlyUrl": friendlyUrl,
    "codeWidgetTwitter": codeWidgetTwitter,
    "chatType": chatType?.toString(),
    "chatModeratorEmail": chatModeratorEmail,
    "viewPageUrl": viewPageUrl,
    "domainsEmbed": domainsEmbed,
    "schedules": schedules?.map((e) => e.toJson()),
    "streamType": streamType?.toString(),
    "institution": institution?.toJson(),
    "apiHost": apiHost,
    "sharedSecret": sharedSecret,
    "meetingID": meetingId,
    "record": record,
    "geolocationControl": geolocationControl?.toString(),
    "channels": channels?.map((e) => e.toJson()),
  };
}
