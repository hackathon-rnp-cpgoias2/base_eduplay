import 'dart:convert';

import 'package:base_eduplay/src/model/channel.dart';
import 'package:base_eduplay/src/model/playlist.dart';
import 'package:base_eduplay/src/model/institution.dart';
import 'package:base_eduplay/src/model/enums.dart';


class VideoResult {
  VideoResult({
    this.qtTotal,
    this.items,
  });

  int? qtTotal;
  List<Video>? items;

  factory VideoResult.fromRawJson(String str) => VideoResult.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory VideoResult.fromJson(Map<String, dynamic> json) => VideoResult(
    qtTotal: json["qtTotal"],
    items: (json["videoList"] as List<dynamic>).map((e) => Video.fromJson(e as Map<String, dynamic>)).toList(growable: false),
  );

  Map<String, dynamic> toJson() => {
    "qtTotal": qtTotal,
    "videoList": items?.map((x) => x.toJson()),
  };
}

class Video {
  Video({
    this.id,
    this.title,
    this.description,
    this.keywords,
    this.friendlyUrl,
    this.downloadable,
    this.visibility,
    this.status,
    this.password,
    this.duration,
    this.embed,
    this.externalIdentifier,
    this.controlInformation,
    this.subtitlePath,
    this.registrationDate,
    this.modificationDate,
    this.videoPublishStatus,
    this.geolocationControl,
    this.deactivationDate,
    this.generateLibras,
    this.generateSubtitle,
    this.domainsEmbed,
    this.images,
    this.institution,
    this.playlists,
    this.channels,
  });

  int? id;
  String? title;
  String? description;
  String? keywords;
  String? friendlyUrl;
  bool? downloadable;
  Visibility? visibility;
  Status? status;
  String? password;
  int? duration;
  String? embed;
  String? externalIdentifier;
  String? controlInformation;
  String? subtitlePath;
  String? registrationDate;
  String? modificationDate;
  VideoPublishStatus? videoPublishStatus;
  GeolocationControl? geolocationControl;
  DateTime? deactivationDate;
  bool? generateLibras;
  bool? generateSubtitle;
  List<dynamic>? domainsEmbed;
  List<Image>? images;
  Institution? institution;
  List<Playlist>? playlists;
  List<Channel>? channels;

  factory Video.fromRawJson(String str) => Video.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Video.fromJson(Map<String, dynamic> json) => Video(
    id: json["id"],
    title: json["title"],
    description: json["description"],
    keywords: json["keywords"],
    friendlyUrl: json["friendlyUrl"],
    downloadable: json["downloadable"],
    visibility: json["visibility"] != null ? Visibility.values.byName(json["visibility"]) : null,
    status: json["status"] != null ? Status.values.byName(json["status"]) : null,
    password: json["password"],
    duration: json["duration"],
    embed: json["embed"],
    externalIdentifier: json["externalIdentifier"],
    controlInformation: json["controlInformation"],
    subtitlePath: json["subtitlePath"],
    registrationDate: json["registrationDate"],
    modificationDate: json["modificationDate"],
    videoPublishStatus: json["videoPublishStatus"] != null ? VideoPublishStatus.values.byName(json["videoPublishStatus"]) : null,
    geolocationControl: json["geolocationControl"] != null ? GeolocationControl.values.byName(json["geolocationControl"]) : null,
    deactivationDate: json["deactivationDate"],
    generateLibras: json["generateLibras"],
    generateSubtitle: json["generateSubtitle"],
    domainsEmbed: json["domainsEmbed"],
    images: json["images"] != null ? (json["images"] as List<dynamic>).map((e) => Image.fromJson(e as Map<String, dynamic>)).toList(growable: false) : null,
    institution: json["institution"] != null ? Institution.fromJson(json["institution"]) : null,
    playlists: json["playlists"] != null ? (json["playlists"] as List<dynamic>).map((e) => Playlist.fromJson(e as Map<String, dynamic>)).toList(growable: false) : null,
    channels: json["channels"] != null ? (json["channels"] as List<dynamic>).map((e) => Channel.fromJson(e as Map<String, dynamic>)).toList(growable: false) : null,
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "description": description,
    "keywords": keywords,
    "friendlyUrl": friendlyUrl,
    "downloadable": downloadable,
    "visibility": visibility?.name,
    "status": status?.name,
    "password": password,
    "duration": duration,
    "embed": embed,
    "externalIdentifier": externalIdentifier,
    "controlInformation": controlInformation,
    "subtitlePath": subtitlePath,
    "registrationDate": registrationDate,
    "modificationDate": modificationDate,
    "videoPublishStatus": videoPublishStatus?.name,
    "geolocationControl": geolocationControl?.name,
    "deactivationDate": deactivationDate,
    "generateLibras": generateLibras,
    "generateSubtitle": generateSubtitle,
    "domainsEmbed": domainsEmbed,
    "images": images?.map((e) => e.toJson()),
    "institution": institution?.toJson(),
    "playlists": playlists?.map((e) => e.toJson()),
    "channels": channels?.map((e) => e.toJson()),
  };
}

class Image {
  Image({
    this.type,
    this.href,
  });

  Type? type;
  String? href;

  factory Image.fromRawJson(String str) => Image.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Image.fromJson(Map<String, dynamic> json) => Image(
    type: json["type"] != null ? Type.values.byName(json["type"]) : null,
    href: json["href"],
  );

  Map<String, dynamic> toJson() => {
    "type": type?.name,
    "href": href,
  };
}

class VideoVersion {
  VideoVersion({
    this.id,
    this.fileFormat,
    this.bitRate,
    this.frameRate,
    this.frameWidth,
    this.frameHeight,
    this.url,
  });

  int? id;
  String? fileFormat;
  int? bitRate;
  double? frameRate;
  int? frameWidth;
  int? frameHeight;
  String? url;

  factory VideoVersion.fromRawJson(String str) => VideoVersion.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory VideoVersion.fromJson(Map<String, dynamic> json) => VideoVersion(
    id: json["id"],
    fileFormat: json["fileFormat"],
    bitRate: json["bitRate"],
    frameRate: json["frameRate"],
    frameWidth: json["frameWidth"],
    frameHeight: json["frameHeight"],
    url: json["url"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "fileFormat": fileFormat,
    "bitRate": bitRate,
    "frameRate": frameRate,
    "frameWidth": frameWidth,
    "frameHeight": frameHeight,
    "url": url,
  };
}
