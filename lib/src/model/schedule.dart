
import 'dart:convert';

class Schedule {
  Schedule({
    this.day,
    required this.startTime,
    required this.endTime,
  });

  dynamic day;
  String startTime;
  String endTime;

  factory Schedule.fromRawJson(String str) => Schedule.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Schedule.fromJson(Map<String, dynamic> json) => Schedule(
    day: json["day"],
    startTime: json["startTime"],
    endTime: json["endTime"],
  );

  Map<String, dynamic> toJson() => {
    "day": day,
    "startTime": startTime,
    "endTime": endTime,
  };
}