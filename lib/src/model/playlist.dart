import 'dart:convert';

class Playlist {
  Playlist({
    this.id,
    this.name,
    this.owner,
    this.member,
  });

  int? id;
  String? name;
  bool? owner;
  bool? member;

  factory Playlist.fromRawJson(String str) => Playlist.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Playlist.fromJson(Map<String, dynamic> json) => Playlist(
    id: json["id"],
    name: json["name"],
    owner: json["owner"],
    member: json["member"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "owner": owner,
    "member": member,
  };
}