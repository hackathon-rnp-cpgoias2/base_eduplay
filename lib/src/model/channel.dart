import 'dart:convert';

class Channel {
  Channel({
    this.id,
    this.name,
    this.owner,
    this.member,
  });

  int? id;
  String? name;
  bool? owner;
  bool? member;

  factory Channel.fromRawJson(String str) => Channel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Channel.fromJson(Map<String, dynamic> json) => Channel(
    id: json["id"],
    name: json["name"],
    owner: json["owner"],
    member: json["member"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "owner": owner,
    "member": member,
  };
}