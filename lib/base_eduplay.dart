library base_eduplay;

import 'package:dio/dio.dart';

export 'src/model/channel.dart';
export 'src/model/enums.dart';
export 'src/model/institution.dart';
export 'src/model/playlist.dart';
export 'src/model/schedule.dart';
export 'src/model/transmission.dart';
export 'src/model/video.dart';

export 'src/repository/video_repository.dart';

Dio _makeDio() {
  final options = BaseOptions(
    baseUrl: 'https://dev.eduplay.rnp.br/services',
    headers: {
      'clientKey':'3247cb085ea21b1bfb9364437302d9ab34a037c38d3181fadee61bed38fbb963',
      'Accept': 'application/json'
    },
    followRedirects: true,
    receiveDataWhenStatusError: true,
    setRequestContentTypeWhenNoPayload: false,
    listFormat: ListFormat.multi,
    connectTimeout: 3600,
    receiveTimeout: 3600,
    sendTimeout: 3600,
  );

  final dio = Dio(options);
  dio.interceptors.add(LogInterceptor(responseBody: true));
  return dio;
}

Dio get eduplayDio => _makeDio();
